def passKeys(raw_dict: dict, keys: list):
    pass_dict = {}
    for k, v in raw_dict.items():
        for white_key in keys:
            if white_key == k:
                pass_dict[k] = v
                break
    return pass_dict


def cutKeys(raw_dict: dict, keys: list):
    pass_dict = {}
    for k, v in raw_dict.items():
        for black_key in keys:
            if black_key == k:
                break
        else:
            pass_dict[k] = v
    return pass_dict
