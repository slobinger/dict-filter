# dict_filter

A module with filter functions for dictionaries.

## how to install

```
pip install dict-filter \
    --extra-index-url https://gitlab.com/api/v4/projects/15047261/packages/pypi/simple
```

## integration in other projects with git submodule

the project directory is already structured as python package.
If one wants to package it for pip, debian, or something else it is possible to
simply use it as git submodule in the destribution specific packaging project.
The same is possible to use the module in your own project if you do not want to
package anything.

```bash
git submodule add $this_project_ref dict_filter
```

## Use

The wording is inspired by filters in synthesizers.
A low-cut-filter can also be described as a high-pass-filter.
Same is for the filter methods of this module. cutKeys returns a dict without
the given keys and passKeys returns a dict with only the given keys.

```python
from dict_filter import dict_filter

raw_dict = {
    username: 'dude'
    oldPassword: 'aA1234Aa',
    newPassword: 'bB5678Bb'}

black_list = ['username']
white_list = black_list

passed_dict = dict_filter.cutKeys(raw_dict = raw_dict, keys = black_list)
print(passed_dict)
# {oldPassword: 'aA1234Aa', newPassword: 'bB5678Bb'}
passed_dict = dict_filter.passKeys(raw_dict = raw_dict, keys = white_list)
print(passed_dict)
# {username: 'dude'}
```

## develoment and build

install in edit mode
```
venv/bin/pip install -e .
```

build
```
venv/bin/python setup.py sdist bdist_wheel
```